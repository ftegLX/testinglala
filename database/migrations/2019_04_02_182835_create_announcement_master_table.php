<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnnouncementMasterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('AnnouncementMaster', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->index();
            $table->string('image', 255);
            $table->boolean('push_notification')->default(false);
            $table->longtext('content');
            $table->string('status', 20)->index();
            $table->timestamps();
            $table->integer('created_by');
            $table->integer('updated_by');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('AnnouncementMaster');
    }
}
