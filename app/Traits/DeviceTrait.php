<?php
namespace Vanguard\Traits;

use Illuminate\Http\Request;
use Vanguard\User;
use Fteg\Device;

trait DeviceTrait {
    public function devices() {
        return $this->hasMany(Device::class);
    }
}
