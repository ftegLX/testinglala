<?php
namespace Vanguard\Traits;

use Illuminate\Http\Request;
use Vanguard\User;
use Fteg\UserInfo;

trait UserInfoTrait {
    public function user_info() {
        return $this->hasOne(UserInfo::class);
    }

    public function set_verified() {
        return $this->user_info()->update(['verified' => true]);
    }

    public function set_registered() {
        return $this->user_info()->update(['is_registered' => true]);
    }
}
