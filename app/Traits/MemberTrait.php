<?php
namespace Vanguard\Traits;

use Illuminate\Http\Request;
use Vanguard\User;
use Fteg\Member;
use Fteg\Merchant;

trait MemberTrait {
    public function memberships() {
        return $this->belongsToMany(Member::class, 'member_user', 'user_id', 'member_id');
    }

    public function add_parent_membership(Member $member) {
        $this->memberships()->attach($member->id, ['level' => Member::LEVEL_PARENT]);
    }

    public function add_child_membership(Member $member) {
        $this->memberships()->attach($member->id, ['level' => Member::LEVEL_CHILD]);
    }

    public function is_member_of(Merchant $merchant) {
        // dd($this->memberships()->where('merchant_id', $merchant->id)->get());
        return $this->memberships()->where('MemberMaster.merchant_id', $merchant->id)->exists();
        // $this->memberships()


        // return $this->memberships()
        //     ->join('member_merchant', 'member_merchant.member_id', 'MemberMaster.id')
        //     ->where('member_merchant.merchant_id', $merchant->id)
        //     ->count() > 0;
    }

    public function is_linked_to_member(Member $member) {
        return $this->memberships()->exists($member->id);
    }
}
