<?php
namespace Vanguard\Serializers;

use League\Fractal\Serializer\DataArraySerializer;

class CustomSerializer extends DataArraySerializer
{
    // public function collection($resourceKey, array $data) {
    //     return $data;
    // }

    // public function item($resourceKey, array $data) {
    //     return $data;
    // }

    public function collection($resourceKey, array $data) {
        if ($resourceKey) {
            return [$resourceKey => $data];
        }
        return $data;
    }

    public function item($resourceKey, array $data)
    {
        if ($resourceKey) {
            return [$resourceKey => $data];
        }
        return $data;
    }


    // public function mergeIncludes($transformedData, $includedData)
    // {
    //     $includedData = array_map(function ($include) {
    //         return $include['data'];
    //     }, $includedData);
    //
    //     return parent::mergeIncludes($transformedData, $includedData);
    // }
}
