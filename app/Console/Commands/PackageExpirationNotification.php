<?php
namespace Vanguard\Console\Commands;

use Fteg\Member;
use Fteg\Merchant;
use Fteg\OrderPackage;
use Carbon\Carbon;
use Fteg\AppNotification;
use Illuminate\Console\Command;

class PackageExpirationNotification extends Command
{
    protected $signature = 'fteg:package_expiry_notification';
    protected $description = 'Send notification to members to inform them on package expiration.';

    public function __construct() {
        parent::__construct();
    }

    public function handle() {
        $this->send_package_expiry_notification();
    }

    protected function send_package_expiry_notification() {
        $today = Carbon::today();
        $reminders = OrderPackage::REMINDER_DAYS;
        // dd(OrderPackage::REMINDER_DAYS);

        foreach ($reminders as $reminder) {
            $reminder_date = $today->copy()->addDays($reminder);
            $this->shared_app_send_notification($reminder_date);


        }
    }

    protected function shared_app_send_notification($date) {
        // retrieve order package for normal level merchant
        $normal_order_packages = $this->order_package_query($date)
                            ->where('MerchantMaster.level', Merchant::LEVEL_NORMAL)
                            ->where('member_user.level', Member::LEVEL_PARENT)
                            ->get();

        // group based on merchant
        $merchant_ids = $normal_order_packages->groupBy('merchant_id');
        // dd($merchant_ids->toArray());
        foreach ($merchant_ids as $merchant_id) {

        }
    }

    protected function order_package_query($date) {
        return OrderPackage::join('OrderMaster', 'OrderMaster.id', '=', 'OrderPackageMaster.order_id')
                ->join('PackageMaster', 'PackageMaster.id', '=', 'OrderPackageMaster.package_id')
                ->join('MemberMaster', 'MemberMaster.id', '=', 'OrderMaster.member_id')
                ->join('MerchantMaster', 'MerchantMaster.id', '=', 'MemberMaster.merchant_id')
                ->join('member_user', 'MemberMaster.id', '=', 'member_user.member_id')
                ->where('OrderPackageMaster.expiry_date', $date->format('Y-m-d'))
                ->select('OrderPackageMaster.*', 'member_user.user_id as user_id', 'OrderMaster.member_id',
                        'OrderMaster.merchant_id', 'MerchantMaster.level as merchant_level',
                        'PackageMaster.name as package_name');
    }


    // protected function shared_order_package($date) {
    //
    // }
}
