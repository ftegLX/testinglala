<?php

/*
upload_file
export_html_pdf
send_mail
get_member_id
send_otp
verify_otp
call_api
unique_code
event_log
*/

use Carbon\Carbon;
use GuzzleHttp\Client;
use Mpdf\Mpdf;
// use File;

function upload_file($file, $upload_path, $file_name = null, $previous = null, $quality = 70) {
    $extension = $file->getClientOriginalExtension();

    if(!@$file_name)
        $file_name = basename(md5(time())).rand(0,999).".".$extension;

    if (!file_exists($upload_path))
        File::makeDirectory($upload_path, 0775, true);

    if (!file_exists($upload_path.$quality.'/'))
        File::makeDirectory($upload_path.$quality.'/', 0775, true);

    if (!file_exists($upload_path.'thumbnails/'))
        File::makeDirectory($upload_path.'thumbnails/', 0775, true);

    $img = Image::make($file);

    $img->save($upload_path.$file_name);
    $img->save($upload_path.$quality.'/'.$file_name, $quality);
    $img->resize(null, 50, function ($constraint) {
        $constraint->aspectRatio();
    })->save($upload_path.'thumbnails/'.$file_name, 10);

    if ($previous != null) {
        if (file_exists($upload_path.$previous))
            unlink($upload_path.$previous);

        if (file_exists($upload_path.$quality.'/'.$previous))
            unlink($upload_path.$quality.'/'.$previous);

        if (file_exists($upload_path.'thumbnails/'.$previous))
            unlink($upload_path.'thumbnails/'.$previous);
    }

    return $file_name;
}

function upload_image_with_json_response($file, $upload_path) {
    $extension = $file->getClientOriginalExtension();

    // File format allow.
    $allowed = array('png', 'jpg', 'gif', 'jpeg');
    // if extensions are not image
    if (!in_array(strtolower($extension), $allowed)) {
        return json_encode((object) array(
            'status' => 'error',
            'message' => 'File type not allowed!',
        ));
        exit;
    }

    $filename = upload_file($file, $upload_path);
    return json_encode((object) [
        'status'    => 'success',
        'file_path' => url($upload_path.'70/'.$filename)
    ]);
}



function delete_file($file) {
    if (file_exists($file))
        unlink($file);
}

// function upload_summernote_image() {
//     $vars  = Input::all();
//     $file = $vars['file'];
//
//     // File format allow.
//     $allowed = array('png', 'jpg', 'gif','zip');
//     // Set Upload path.
//     $destinationPath = base_path()."/public/".Announcement::$uploadPath;
//     // Get the file extension.
//     $extension = $file->getClientOriginalExtension();
//     // If file format is not compatible, display error.
//     if(!in_array(strtolower($extension), $allowed)){
//         echo '{"status":"error"}';
//         exit;
//     }
//     // Rename the upload file.
//     $filename = md5(time().rand(1111,99999)).'.'.$extension;
//     // Move the uploaded files to the destination directory.
//     $file->move($destinationPath, $filename);
//
//     return '{"status":"success","file_path":"'.url(Announcement::$uploadPath.$filename).'"}';
// }

// Compress
// function compressImage($source, $destination, $quality = 70) {
//     $info = getimagesize($source);
//     $mime = $info['mime'];
//
//     switch ($mime) {
//         case 'image/jpeg':
//             $image = imagecreatefromjpeg($source);
//             break;
//         case 'image/png':
//             $image = imagecreatefrompng($source);
//             break;
//         case 'image/gif':
//             $image = imagecreatefromgif($source);
//             break;
//         default:
//             return [
//                 'status'    => 0,
//                 'msg'       => 'Unknown type of image selected.',
//             ];
//             break;
//     }
//     imagejpeg($image, $destination, $quality);
//     return ['status' => 1, 'msg' => 'Successful compressed image.'];
// }

function export_html_pdf($html_array, $watermark = null, $upload_path = null, $file_name = null){
    $mpdf = new \Mpdf\Mpdf();
    // $mpdf->SetWatermarkImage(env('WEB_DOMAIN').env('LOGO'), 0.2, array(180,50));
    // Set Watermark Image
    if(@$watermark) {
    	$mpdf->SetWatermarkImage($watermark['image'], $watermark['opacity'], $watermark['position']);
    	$mpdf->showWatermarkImage = true;
	}
    $mpdf->WriteHTML($html_array[0]); // For the default 1st page

    // Add Page
    if(count($html_array)>1) {
        for($i = 1; $i < count($html_array); $i++){
            $mpdf->AddPage();
            $mpdf->WriteHTML($html_array[$i]);
        }
    }

    // Show Pdf or Store Pdf
    // Future Enhancement -> \Mpdf\Output\Destination::INLINE (show only in the browser)
    if(@$upload_path) {
    	if(!@$file_name) $file_name = basename(md5(time())).rand(0,999).".pdf";
    	$mpdf->Output('.'.$upload_path.'/'.$file_name, \Mpdf\Output\Destination::FILE);
    } else {
    	$mpdf->Output();
    }
}

// function send_mail($from, $to, $title, $template, $content) {
//     Mail::send($template, compact('content'), function ($message) use ($from, $to, $title) {
//         $message->from($from);
//         $message->to($to);
//         $message->subject($title);
//     });
// }

function get_member_id(){
    if(!Auth::check()) return false;
    return DB::table('MemberMaster')->where('user_id', Auth::user()->id)->value('id');
}

function created_by($member_id=null){
    if(!@$member_id && Auth::check()) return Auth::user()->id;
    return DB::table('MemberMaster')->where('id', $member_id)->value('user_id');
}

function send_otp($phone, $ip, $channel, $merchant_id = null) {
    $data = [];
    $data['token'] = md5(env('OTP_AGENT_ID').env('OTP_AGENT_KEY'));

    if ($merchant_id != null) {
        $merchant = \Fteg\Merchant::find($merchant_id);
        $data['token'] = md5($merchant->otp_agent_id.$merchant->otp_agent_key);
    }

    $data['phone'] = $phone;
    $data['ip'] = $ip;
    $data['channel'] = $channel;
    $result = call_api('post', env('OTP_DOMAIN').'/api/password/generate', $data);
    if($result['status'] == 0) return $result;

    $result['status'] = 1;
    $result['msg'] = 'Verification code has sent to your phone number. Please input into the verification box.';
    return $result;
}

function verify_otp($phone, $password, $merchant_id = null){
    $data = [];
    $data['token'] = md5(env('OTP_AGENT_ID').env('OTP_AGENT_KEY'));

    if ($merchant_id != null) {
        $merchant = \Fteg\Merchant::find($merchant_id);
        $data['token'] = md5($merchant->otp_agent_id.$merchant->otp_agent_key);
    }
    
    $data['phone'] = $phone;
    $data['password'] = $password;
    $result = call_api('post', env('OTP_DOMAIN').'/api/password/verify', $data);
    return $result;
}

function call_api($method, $url, $data){
    $client = new Client();
    $res = $client->request($method, $url, ['query'=>$data]);
    return (array)json_decode($res->getBody());
}

function unique_code($table, $column, $prefix=null, $length = 8){
    $continue_create = true;
    while($continue_create){
        $code = str_random($length);
        // $code = sprintf('%07d', $code);
        if(@$prefix) $code = $prefix.$code;
        $sql = "SELECT * FROM `$table` WHERE $column = '$code' ";
        $check = DB::select($sql);
        if($check) break;
        if(!$check) $continue_create = false;
    }
    return $code;
}

// function event_log($type, $id, $update_data = null){
//     $note = '';
//     $create = false;
//     switch ($type) {
//         case 'update_profile':
//             break;
//         default:
//             return false;
//             break;
//     }
//     if(!@$create) return false;
//
//     $store_data = [];
//     $store_data['type'] = $type;
//     $store_data['table'] = $table;
//     $store_data['id_table'] = $table_id;
//     $store_data['note'] = $note;
//     $store_data['created_by'] = Auth::user()->id;
//     $store_data['created_on'] = Carbon::now();
//     DB::table('EventLog')->insert($store_data);
//
//     return;
// }

// function push_notification($array_player_ids = [], $array_topics = [], $notification_data = []){
//     // $content = array(
//     //     "en" => 'English Message'
//     //     );
//
//     $fields = array(
//         'app_id' => env('APP_ID'),
//         // 'included_segments' => array('Active Users'), // topic, replace Active Users to All (send to all)
//         'include_player_ids' => $array_player_ids,
//         'data' => $notification_data,
//         'headings' => ["en"=>$notification_data['title']],
//         'contents' => ["en"=>$notification_data['message']]
//         //$content
//     );
//
//     $fields = json_encode($fields);
//     // print("\nJSON sent:\n");
//     // print($fields);
//
//     $ch = curl_init();
//     curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
//     curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
//                                                'Authorization: Basic '.env('REST_API_KEY')));
//     curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
//     curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
//     curl_setopt($ch, CURLOPT_HEADER, FALSE);
//     curl_setopt($ch, CURLOPT_POST, TRUE);
//     curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
//     curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
//
//     $response = curl_exec($ch);
//     curl_close($ch);
//
//     // $store = [];
//     // $store['multicast_id'] = json_encode($array_player_ids);
//     // $store['results'] = json_encode($response);
//     // $store['message'] = json_encode($notification_data);
//     // $store['created_on'] = Carbon::now();
//     // DB::table('NotificationTrx')->insert($store);
//
//     return $response;
// }
