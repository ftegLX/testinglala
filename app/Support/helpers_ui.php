<?php
function sidebar($key = null, $default = null){
    $data = json_decode(file_get_contents('../storage/sidebar.json'));
    return (@$key)?$data->$key:$data;
}

// function delete_button($module, $id, $url_prefix=null) {
//     $url_prefix = $url_prefix ? "/".$url_prefix : '';
//     return '<a href="'.$url_prefix.'/'.$module.'/'.$id.'" class="btn btn-icon" title="" data-toggle="tooltip" data-placement="top" data-method="DELETE" data-confirm-title="Please Confirm" data-confirm-text="Are you sure that you want to delete this '.str_singular($module).'?" data-confirm-delete="Yes" data-original-title="Delete '.ucwords($module).'">
//             <i class="fas fa-trash"></i>
//         </a>';
// }

function delete_button($url, $item_name) {
    return '<a href="'.$url.'" class="btn btn-icon" title="" data-toggle="tooltip" data-placement="top" data-method="DELETE" data-confirm-title="Please Confirm" data-confirm-text="Are you sure that you want to delete this '.str_singular($item_name).'?" data-confirm-delete="Yes" data-original-title="Delete '.ucwords($item_name).'">
            <i class="fas fa-trash"></i>
        </a>';
}

// function edit_button($module, $id, $url_prefix=null) {
//     $url_prefix = $url_prefix ? "/".$url_prefix : '';
//     return '<a href="'.$url_prefix.'/'.$module.'/'.$id.'/edit" class="btn btn-icon edit" title="Edit"><i class="fas fa-edit"></i></a>';
// }

function edit_button($url) {
    return '<a href="'.$url.'" class="btn btn-icon edit" title="Edit"><i class="fas fa-edit"></i></a>';
}

function status_ui($status){
    switch ($status) {
        case 'Active':
        case 'Approved':
        case 'Confirmed':
        case 'Paid':
        case 'Success':
            $ui = '<label class="badge badge-success">'.$status.'</label>';
            break;

        case 'Inactive':
        case 'Resigned':
        case 'Expired':
        case 'Forfeited':
        case 'Banned':
        case 'Used':
            $ui = '<label class="badge badge-danger">'.$status.'</label>';
            break;
        case 'Unpaid':
        case 'Pending':
        case 'Probationer':
            $ui = '<label class="badge badge-warning">'.$status.'</label>';
            break;
        case 'Completed':
        case 'Published';
            $ui = '<label class="badge badge-success"><i class="fa fa-check"></i> '.$status.'</label>';
            break;

        case 'Rejected':
            $ui = '<label class="badge badge-danger"><i class="fas fa-ban"></i> '.$status.'</label>';
            break;

        case 'Cancelled':
            $ui = '<label class="badge badge-danger"><i class="fas fa-times"></i> '.$status.'</label>';
            break;

        case '1':
            $ui = '<i class="fa fa-check text-success"></i>';
            break;
        case '0':
            $ui = '<i class="fa fa-times text-danger"></i>';
            break;
        default:
            $ui = '<label class="badge badge-default">'.$status.'</label>';
            break;
    }
    return $ui;
}

function wysiwyg_editor_scripts() {
    return '
        <link media="all" type="text/css" rel="stylesheet" href="/assets/plugins/summernote/summernote-lite.css">
        <script src="/assets/plugins/summernote/summernote-lite.js"></script>
    ';
}

function select2_scripts() {
    return '<link media="all" type="text/css" rel="stylesheet" href="'.url('assets/plugins/select2/css/select2.min.css').'">'.
        '<script src="'.url('assets/plugins/select2/js/select2.min.js').'"></script>';
}

function daterange_scripts(){
    return
    '<link media="all" type="text/css" rel="stylesheet" href="/assets/plugins/bootstrap-daterangepicker-master/daterangepicker.css">
    <script src="/assets/plugins/bootstrap-daterangepicker-master/moment.min.js"></script>
    <script src="/assets/plugins/bootstrap-daterangepicker-master/daterangepicker.js"></script>
    <script src="/assets/plugins/bootstrap-daterangepicker-master/daterangepicker_custom.js"></script>';
}

function datatable_scripts(){
    return '<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/zf/dt-1.10.18/datatables.min.css"/>
    <script type="text/javascript" src="https://cdn.datatables.net/v/zf/dt-1.10.18/datatables.min.js"></script>';
}

function datepicker_scripts() {
    return '<link rel="stylesheet" type="text/css" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"/>'.
    '<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js" integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU=" crossorigin="anonymous"></script>';
}

?>
