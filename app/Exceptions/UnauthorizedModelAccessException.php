<?php
namespace Vanguard\Exceptions;

use Exception;

class UnauthorizedModelAccessException extends Exception {

    public function __construct() {
        parent::__construct();
    }

    public function render($request) {
        return response()->view('errors.403', ['exception' => $this]);
    }
}
