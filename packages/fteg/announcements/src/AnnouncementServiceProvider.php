<?php
namespace Fteg\Announcements;

use Carbon\Carbon;
use Fteg\Announcement;
// use Fteg\Announcements\Observers\AnnouncementObserver;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Validator;

class AnnouncementServiceProvider extends ServiceProvider
{
    public function boot() {
        // Announcement::observe(AnnouncementObserver::class);
        $this->loadViewsFrom(__DIR__.'/views', 'announcement');
    }

    public function register() {
        $this->loadRoutesFrom(__DIR__.'/routes/web.php');
        $this->loadRoutesFrom(__DIR__.'/routes/v1/api.php');
    }
}
