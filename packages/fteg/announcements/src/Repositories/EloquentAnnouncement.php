<?php
namespace Fteg\Announcements\Repositories;

use Illuminate\Support\Facades;
use Illuminate\Http\Request;

use Auth;
use Schema;
use Carbon\Carbon;
use Fteg\Announcement;
use Fteg\Merchant;
use Fteg\NotificationHistory;

class EloquentAnnouncement
{
    // ********************** QUERY ********************** //
    public function find($id) {
        return Announcement::find($id);
    }

    public function list_active_10() {
        return Announcement::active()->limit(10);
    }

    public function list($paginate = null, Request $request) {
        $query = Announcement::query();
        // dd($request->title);
        if ($request->has('title') && $request->title != '')
            $query->whereRaw('lower(AnnouncementMaster.title) like "%'.strtolower($request->title).'%"');

        if ($request->has('status') && $request->status != '')
            $query->where('AnnouncementMaster.status' , $request->status);

        if ($request->has('sort') && $request->sort != '') {
            $sort_type = $this->get_sort_type($request);
            $query->orderBy('AnnouncementMaster.'.$request->sort, $sort_type);
        } else {
            $query->orderBy('AnnouncementMaster.created_at', 'desc');
        }

        if ($paginate)
            return $query->paginate();

        return $query->get();
    }

    // ********************** CRUD ********************** //
    public function create(Request $request) {
        $data = $request->all();
        $data['created_by'] = auth()->id();

        if ($request->hasFile('image'))
            $data['image'] = upload_file($request->file('image'), Announcement::UPLOAD_PATH);

        return Announcement::create($data);
    }

    public function update(Request $request, Announcement $announcement) {
        $data = $request->all();
        $data['updated_by'] = auth()->id();

        if ($request->hasFile('image'))
            $data['image'] = upload_file($request->file('image'), Announcement::UPLOAD_PATH, null, $announcement->image);

        $announcement->update($data);

        // remove all inactive notifications
        // remove previous notification
        // if ($data['status'] != Announcement::STATUS_ACTIVE || $request->push_notification) {
        //     NotificationHistory::where('module', 'announcement')->where('module_id', $announcement->id)
        //         ->update(['status' => NotificationHistory::STATUS_REMOVED]);
        // }
    }

    public function delete(Announcement $announcement) {
        $path = Announcement::UPLOAD_PATH.$announcement->image;
        $compress_path =  Announcement::UPLOAD_PATH_70.$announcement->image;
        $thumbnail_path =  Announcement::UPLOAD_PATH_THUMBNAIL.$announcement->image;

        if ($announcement->getOriginal('image') != null) {
            delete_file($path);
            delete_file($compress_path);
            delete_file($thumbnail_path);
        }
        return $announcement->delete();
    }

    protected function get_sort_type(Request $request) {
        return $request->sort_type ?: 'desc';
    }
}
