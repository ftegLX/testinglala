<?php
namespace Fteg\Announcements\Transformers;

use League\Fractal\TransformerAbstract;
use Fteg\Announcement;

class AnnouncementTransformer extends TransformerAbstract
{
    public function transform(Announcement $announcement) {
        return [
            'id' => $announcement->id,
            'title' => $announcement->title,
            'image' => $announcement->present()->image,
            'image_thumbnail' => $announcement->present()->image_thumbnail,
            'content' => $announcement->present()->content,
            'status' => $announcement->status,
            'created_at' => $announcement->present()->created_at
        ];
    }
}
