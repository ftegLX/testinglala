<?php
namespace Fteg;

use Fteg\Merchant;

use Vanguard\Exceptions\UnauthorizedModelAccessException;
use Illuminate\Database\Eloquent\Model;
use Laracasts\Presenter\PresentableTrait;

class Announcement extends Model
{
    use PresentableTrait;

    protected $table = 'AnnouncementMaster';
    protected $presenter = 'Fteg\Announcements\Presenters\AnnouncementPresenter';
    protected $fillable = ['title', 'content', 'push_notification', 'status', 'image', 'created_by', 'updated_by'];
    protected $perPage = 10;

    const UPLOAD_PATH = 'upload/announcements/';
    const UPLOAD_PATH_70 = 'upload/announcements/70/';
    const UPLOAD_PATH_THUMBNAIL = 'upload/announcements/thumbnails/';

    // constants
    const STATUS_ACTIVE = 'Active';
    const STATUS_INACTIVE = 'Inactive';

    // constant arrays
    const STATUSES = array(
        self::STATUS_ACTIVE,
        self::STATUS_INACTIVE
    );

    // eg usage: Announcement::active()
    public function scopeActive($query){
        return $query->where('AnnouncementMaster.status', self::STATUS_ACTIVE);
    }

    public function scopeInactive($query){
        return $query->where('AnnouncementMaster.status', self::STATUS_INACTIVE);
    }
}
