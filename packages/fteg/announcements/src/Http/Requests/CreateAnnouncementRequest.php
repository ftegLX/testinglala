<?php
namespace Fteg\Http\Requests\Announcements;

use Vanguard\Http\Requests\FormRequest;
use Fteg\Announcement;

class CreateAnnouncementRequest extends FormRequest
{
    public function authorize() {
        return true;
    }

    public function rules() {
        return [
           'title'         => 'required',
           'image'         => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:1024',
           'status'        => 'required|in:'.implode(',', Announcement::STATUSES),
           'content'       => 'required',
        ];
    }

   public function messages(){
        return [
            'image.image' => 'The image field must of an image type.',
            'image.max' => 'Maximum allowed file size is 1mb.'
        ];
   }

}
