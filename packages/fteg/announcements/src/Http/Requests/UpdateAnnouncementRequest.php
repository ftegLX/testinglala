<?php
namespace Fteg\Http\Requests\Announcements;

use Vanguard\Http\Requests\Request;
use Fteg\Announcement;

class UpdateAnnouncementRequest extends Request
{
   public function rules() {
        $data = [
            'title'         => 'sometimes|required',
            'image'         => 'sometimes|image|max:1024',
            'status'        => 'sometimes|in:'.implode(',', Announcement::STATUSES),
            'content'       => 'sometimes',
        ];
        return $data;
   }

   public function messages(){
        return [
            'image.image' => 'The image field must of an image type.',
            'image.max' => 'Maximum allowed file size is 1mb.'
        ];
   }

}
