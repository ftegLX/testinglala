<?php
namespace Fteg\Http\Requests\Announcements;

use Vanguard\Http\Requests\FormRequest;
use Fteg\Announcement;

class ListAnnouncementRequest extends FormRequest
{
    public function authorize() {
        return true;
    }

    public function rules() {
        return [
           'merchant_id' => 'sometimes|exists:MerchantMaster,id',
           'title' => 'nullable|string',
           'status' => 'nullable|in:Active,Inactive',
           'sort' => 'nullable|in:title,push_notification,status,created_at,updated_at,created_by,updated_by',
           'sort_type' => 'nullable|in:asc,desc'
        ];
    }

   public function messages(){
        return [

        ];
   }

}
