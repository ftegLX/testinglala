@extends('layouts.app')

@section('page-title', trans($title))
@section('page-heading', trans($title))

@section('breadcrumbs')
<li class="breadcrumb-item active">
    Announcements
</li>
@stop

@section('content')
@include('partials.messages')
<div class="card">
    <div class="card-body">
        @permission('create.announcements')
        <a href="/announcements/create" class="btn btn-primary btn-rounded" style="margin-bottom: 10px">
            <i class="fas fa-plus mr-2"></i> Add
        </a>
        @endpermission
        <form action="" method="GET" id="search-form">
            <div class="row float-right">
                <div class="flex-column px-2">
                    <div class="form-group">
                        <label class="control-label">Title</label>
                        <div class="input-group">
                            <input class="form-control search-input" placeholder="Title" name="title" type="text" value="{{ Input::get('title') }}">
                        </div>
                    </div>
                </div>
                <div class="flex-column px-2">
                    <div class="form-group">
                        <label class="control-label">Status</label>
                        <select class="form-control search-input" name="status">
                            <option value="">-- Status --</option>
                            @foreach (\Fteg\Announcement::STATUSES as $status)
                                <option value="{{ $status }}" {{ Input::get('status') == $status ? 'selected' : '' }}>{{ $status }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                @if (Request::has('title') || Request::has('merchant_id'))
                    <div class="flex-column px-2">
                        <div class="form-group">
                            <label for="search" class="control-label">&nbsp;</label>
                            <div class="input-group">
                                <a href="/announcements" id="search" class="btn-danger form-control text-center">
                                    <i class="fas fa-times"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                @endif
                <div class="flex-column px-2">
                    <div class="form-group">
                        <label for="search" class="control-label">&nbsp;</label>
                        <div class="input-group">
                            <a href="javascript:{}" id="search" class="btn-primary form-control text-center" onclick="document.getElementById('search-form').submit(); return false;">
                                <i class="fas fa-search mr-2"></i>Search
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        @permission('view.announcements')
        <div class="table-responsive">
            <table id="table" class="table table-borderless table-striped">
            	<thead>
                    <th style="width:50px"></th>
                    <th>Title</th>
                    <th>Created At</th>
                    <th>Status</th>
                    <th style="width:130px">Action</th>
            	</thead>
                <tbody>
                    @if ($announcements->count() == 0)
                        <tr><td colspan="5" class="text-center">No data</td></tr>
                    @endif
                    @php $count = $announcements->firstItem(); @endphp
                    @foreach ($announcements as $announcement)
                        <tr>
                            <td>{{ $count++ }}</td>
                            <td>{{ $announcement->title }}</td>
                            <td>{{ $announcement->present()->created_at }}</td>
                            <td>{!! $announcement->present()->status !!}</td>
                            <td>
                                @permission('edit.announcements')
                                    {!! edit_button('/announcements/'.$announcement->id.'/edit') !!}
                                @endpermission

                                @permission('delete.announcements')
                                {!! delete_button('/announcements/'.$announcement->id, 'announcement') !!}
                                @endpermission
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div class="float-right">
            {{ $announcements->links() }}
        </div>
        @endpermission
    </div>
</div>

@stop

@section('styles')

@stop

@section('scripts')
@stop
