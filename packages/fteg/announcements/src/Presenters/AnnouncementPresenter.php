<?php
namespace Fteg\Announcements\Presenters;

use Laracasts\Presenter\Presenter;
use Fteg\Announcement;
use Carbon\Carbon;

class AnnouncementPresenter extends Presenter
{
    public function image() {
        if (!$this->entity->image)
            return '';

        return url(Announcement::UPLOAD_PATH_70.$this->entity->image);
    }

    public function image_thumbnail() {
        if (!$this->entity->image)
            return '';

        return url(Announcement::UPLOAD_PATH_THUMBNAIL.$this->entity->image);
    }

    public function status() {
        return status_ui($this->entity->status);
    }

    public function created_at() {
        return Carbon::parse($this->entity->created_at)->format('Y-m-d H:i:s');
    }

    public function updated_at() {
        return Carbon::parse($this->entity->created_at)->format('Y-m-d H:i:s');
    }

    public function content() {
        $contentHtml = $this->entity->content;
        $start_needle = '<iframe';
        $end_needle = '</iframe>';

        $start_positions = array();
        $end_positions = array();

        $lastPos = 0;
        while (($lastPos = strpos($contentHtml, $start_needle, $lastPos)) !== false) {
            $div = '<div class="aspect-ratio">';
            $position = $lastPos;
            $lastPos = $lastPos + strlen($start_needle) + strlen($div);
            $contentHtml = substr_replace($contentHtml, $div, $position, 0);
        }

        $lastPos = 0;
        while (($lastPos = strpos($contentHtml, $end_needle, $lastPos))!== false) {
            $div = '</div>';
            $position = $lastPos;
            $lastPos = $lastPos + strlen($end_needle) + strlen($div);
            $contentHtml = substr_replace($contentHtml, $div, $position + strlen('</iframe>'), 0);
        }

        return "<html><head><meta name='viewport' content='width=device-width, initial-scale=1,maximum-scale=1,user-scalable=no'><style>img {width:100%;} iframe {width: 100% !important;} .aspect-ratio { position:relative; padding-top:56.25%; } .aspect-ratio iframe { position: absolute; width: 100%; height: 100%; left: 0; top: 0; } @font-face {font-family: GothamBook; src: url('".url('assets/webfonts/GothamBookRegular.otf')."') format('opentype');}</style></head><body style='font-family: GothamBook'>{$contentHtml}<p><br></p></body></html>";
    }
}
