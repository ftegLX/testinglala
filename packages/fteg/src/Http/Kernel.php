<?php

namespace Fteg\Http;

use Illuminate\Foundation\Http\Kernel as HttpKernel;

class Kernel extends HttpKernel
{
    /**
     * The application's global HTTP middleware stack.
     *
     * These middleware are run during every request to your application.
     *
     * @var array
     */
    protected $middleware = [
    ];
    /**
     * The application's route middleware groups.
     *
     * @var array
     */
    protected $middlewareGroups = [
    ];

    /**
     * The application's route middleware.
     *
     * @var array
     */
    protected $routeMiddleware = [
        'receive' => \Fteg\Middleware\Receive::class,
        'response' => \Fteg\Middleware\Response::class,
        // 'signature' => \Fteg\Middleware\SignatureAuth::class,
        'signature' => \Fteg\Middleware\SignatureAuth::class,
        'maintenance' => \Fteg\Middleware\Maintenance::class,
        'jwt' => \Fteg\Middleware\JwtLoginAuth::class,
        'admin_access_only' => \Fteg\Middleware\AdminAccess::class,
        'merchant_access_only' => \Fteg\Middleware\MerchantAccess::class,
        'has_module' => \Fteg\Middleware\ModuleCheck::class
    ];
}
