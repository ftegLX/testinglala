<?php
//
// namespace Fteg\Middleware;
//
// use Closure;
// use Illuminate\Contracts\Auth\Guard;
// use Carbon\Carbon;
// use Illuminate\Routing\UrlGenerator;
// use Request;
// use Route;
//
// class SignatureAuth
// {
//     protected $auth;
//     /**
//      * Creates a new instance of the middleware.
//      *
//      * @param Guard $auth
//      */
//     public function __construct(Guard $auth)
//     {
//         $this->auth = $auth;
//     }
//
//     /**
//      * Handle an incoming request.
//      *
//      * @param  \Illuminate\Http\Request  $request
//      * @param  \Closure  $next
//      * @return mixed
//      */
//     public function handle($request, Closure $next)
//     {
//         $signature = $request->signature;
//         $secret_key = env('API_SECRET_KEY');
//         $valid = $this->verify_signature($request);
//
//         if ($valid['status'] == 0) {
//             $data = array(
//                 'status'    => 0,
//                 'msg'       => 'Invalid Signature',
//                 'signature' => $valid['signature'],
//                 'signature_from_mobile' => $signature
//             );
//             return response($data);
//         }
//         return $next($request);
//     }
//
//     public function verify_signature($request)
//     {
//         $valid = 0;
//         $signature = request('signature');
//
//         // match request based on call method
//         switch ($request) {
//             case $request->isMethod('get'):
//                 $real_signature = $this->get_method_signature();
//                 break;
//
//             case $request->isMethod('post'):
//                 $real_signature = $this->post_method_signature();
//                 break;
//
//             case $request->isMethod('patch'):
//                 $real_signature = $this->patch_method_signature();
//                 break;
//
//             case $request->isMethod('put'):
//                 $real_signature = $this->put_method_signature();
//                 break;
//
//             case $request->isMethod('delete'):
//                 $real_signature = $this->delete_method_signature();
//                 break;
//
//             default:
//                 $real_signature = 0;
//                 break;
//         }
//
//         if(@$real_signature && @$signature == @$real_signature) $valid = 1;
//         return ['status' => $valid, 'signature' => $real_signature];
//     }
//
//
//     protected function get_method_signature() {
//         // $id = request()->id;
//         $path = request()->path();
//         // dd($path);
//         // dd("Route".request()->route('announcement')->id);
//         switch($path) {
//             // case 'api/merchant/announcements/'.request()->route('announcement')->id:
//             case 'api/home':
//             case 'api/merchants':
//             case 'api/profile':
//             case 'api/announcements/list':
//             case 'api/announcements/details':
//             case 'api/packages/details':
//             case 'api/merchants/'.@request()->route('merchant')->id.'/tiers':
//             case 'api/merchants/'.@request()->route('merchant')->id.'/contact-us':
//             case 'api/notifications/list':
//                 return md5(env('API_SECRET_KEY'));
//                 break;
//
//             case 'api/packages/list':
//             case 'api/my-packages/list':
//             case 'api/my-packages/transactions/list':
//                 return md5(request('merchant_id').env('API_SECRET_KEY'));
//                 break;
//
//             case 'api/my-packages/package-transactions/list':
//                 return md5(request('merchant_id').request('package_owned_id').env('API_SECRET_KEY'));
//                 break;
//             default:
//                 break;
//         }
//     }
//
//     protected function post_method_signature() {
//         // $id = request()->id;
//         $path = request()->path();
//         switch($path) {
//             case 'api/auth/login':
//                 return md5(request('password').env('API_SECRET_KEY'));
//                 break;
//
//             case 'api/check/version':
//                 return md5(request('app_version').env('API_SECRET_KEY'));
//                 break;
//
//             case 'api/auth/register':
//                 return md5(request('phone').request('password').request('confirm_password').env('API_SECRET_KEY'));
//                 break;
//
//             case 'api/auth/otp':
//                 return md5(request('phone').request('channel').env('API_SECRET_KEY'));
//                 break;
//
//             case 'api/auth/otp-verification':
//                 return md5(request('phone').request('password').request('otp').env('API_SECRET_KEY'));
//                 break;
//
//             case 'api/forgot-password':
//                 return md5(request('email').env('API_SECRET_KEY'));
//                 break;
//
//             case 'api/members/accept-invitation':
//                 return md5(request('accept').env('API_SECRET_KEY'));
//                 break;
//
//             case 'api/auth/logout':
//             case 'api/announcements/create':
//             // case 'api/auth/merchants/logout':
//                 return md5(env('API_SECRET_KEY'));
//                 break;
//
//             case 'api/members/verify':
//                 return md5(request('merchant_id').request('member_id').env('API_SECRET_KEY'));
//                 break;
//
//             case 'api/members/add':
//                 return md5(request('member_id').env('API_SECRET_KEY'));
//                 break;
//
//             case 'api/orders':
//             case 'api/my-packages/redeem':
//                 return md5(request('merchant_id').request('code').request('action').env('API_SECRET_KEY'));
//                 break;
//
//             default:
//                 break;
//         }
//     }
//
//     protected function patch_method_signature() {
//         $path = request()->path();
//
//         switch($path) {
//             case 'api/profile/complete':
//                 return md5(request('full_name').request('email').request('birthday').request('gender').env('API_SECRET_KEY'));
//                 break;
//
//             case 'api/profile':
//             case 'api/notifications/read':
//                 return md5(env('API_SECRET_KEY'));
//                 break;
//
//             case 'api/profile/password':
//                 return md5(request('old_password').request('password').request('confirm_password').env('API_SECRET_KEY'));
//                 break;
//
//             default:
//                 break;
//         }
//     }
//
//     protected function put_method_signature() {
//         $path = request()->path();
//
//         switch($path) {
//             case 'api/announcements/update':
//                 return md5(@request('id').env('API_SECRET_KEY'));
//                 break;
//
//             default:
//                 break;
//         }
//     }
//
//     protected function delete_method_signature() {
//         $path = request()->path();
//
//         switch($path) {
//             // case 'api/announcements':
//             //     return md5(env('API_SECRET_KEY'));
//             default:
//                 break;
//         }
//     }
//

}
