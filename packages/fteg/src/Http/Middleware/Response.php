<?php

namespace Fteg\Middleware;

use Closure;
use DB;
use Carbon\Carbon;
use Fteg\ApiAccessLog;

class Response
{

    /**
     * Create a new filter instance.
     *
     * @param  Guard  $auth
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = $next($request);
        $log_id = $request->input('log_id');
        ApiAccessLog::where('id', $log_id)->where('response_status', '')->update([
            'response_data' => json_encode(@$response->original)
        ]);
        
        return $response;
    }
}
