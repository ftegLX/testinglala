<?php

namespace Fteg\Middleware;

use Closure;
// use Illuminate\Contracts\Auth\Guard;
use JWTAuth;
// use Vanguard\User;
// use Fteg\Mobile\Repositories\EloquentDevice;
// use Auth;

use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;
use Tymon\JWTAuth\Exceptions\TokenBlacklistedException;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Http\Middleware\BaseMiddleware;

class JwtLoginAuth extends BaseMiddleware
{
    public function __construct() {

    }

    /**
    * Handle an incoming request.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  \Closure  $next
    * @return mixed
    */
    public function handle($request, Closure $next) {
        // Check for token requested.
        if (!JWTAuth::getToken())
            return response()->json(['status' => 0, 'msg' => 'Token missing.', 'logout' => 1]);

        try {
            JWTAuth::parseToken()->authenticate();
            // $newToken = JWTAuth::refresh();
        } catch (TokenBlacklistedException $e) {
            return response()->json(['status' => 0, 'msg' => 'Your account has been logged in on another device. Kindly contact Admin if you did not perform that.', 'logout' => 1]);
        } catch (TokenExpiredException $e) {
            return response()->json(['status' => 0, 'msg' => 'Your session has expired. Please login again.', 'logout' => 1]);
        } catch (JWTException $e) {
            return response()->json(['status' => 0, 'msg' => 'Your session is no longer valid. Please login again.', 'logout' => 1]);
        }
        return $next($request);
        // Defined response header.
        // $response = $next($request);

        // Send the refreshed token back to the client.
        // return $this->setAuthenticationHeader($response, $newToken);
    }
}
