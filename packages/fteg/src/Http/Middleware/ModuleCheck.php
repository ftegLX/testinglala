<?php

namespace Fteg\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;

class ModuleCheck
{
    protected $auth;

    public function __construct(Guard $auth) {
        $this->auth = $auth;
    }

    public function handle($request, Closure $next, $module = null) {
        // check if user has module
        if ($request->wantsJson())
            $merchant = auth()->merchant();
        else
            $merchant = session('merchant');
            
        // $merchant = session('merchant');
        if (!$merchant->hasModule($module)) {
            if ($request->wantsJson()) {
                return response()->json(['status' => 0, 'msg' => 'Unauthorized.']);
            }
            abort(401, 'Unauthorized!');
        }


        return $next($request);
    }
}
