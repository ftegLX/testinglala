<?php

namespace Fteg\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use Fteg\Merchant;

class MerchantAccess
{
    protected $auth;

    public function __construct(Guard $auth) {
        $this->auth = $auth;
    }

    public function handle($request, Closure $next) {
        // check if user has the permitted role
        if ($this->auth->user()->hasRole('Merchant') || $this->auth->user()->hasRole('Staff'))
            return $next($request);

        abort(401, 'Unauthorized!');
    }
}
