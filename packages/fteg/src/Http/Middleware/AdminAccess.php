<?php

namespace Fteg\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;

class AdminAccess
{
    protected $auth;

    public function __construct(Guard $auth) {
        $this->auth = $auth;
    }

    public function handle($request, Closure $next) {
        if (!$this->auth->user()->hasRole('Admin'))
            abort(401, 'Unauthorized!');

        return $next($request);
    }
}
