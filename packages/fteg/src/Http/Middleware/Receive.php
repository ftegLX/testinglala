<?php

namespace Fteg\Middleware;

use Closure;
use DB;
use Carbon\Carbon;
use Schema;
use Fteg\ApiAccessLog;

class Receive
{

    /**
     * Create a new filter instance.
     *
     * @param  Guard  $auth
     * @return void
     */
    public function __construct()
    {
        // $this->setUp();
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $request->request->add(['method'=>$request->method()]);

        $log_id = ApiAccessLog::create([
            'ip' => $request->ip(),
            'url' => $request->fullUrl(),
            'receive_data' => json_encode($request->all()),
        ])->id;

        $request->request->add(['log_id'=>$log_id]);

        return $next($request);
    }
}
