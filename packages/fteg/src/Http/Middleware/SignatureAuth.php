<?php
namespace Fteg\Middleware;

use Closure;
use Request;
use Illuminate\Contracts\Auth\Guard;

class SignatureAuth
{
    protected $auth;
    /**
     * Creates a new instance of the middleware.
     *
     * @param Guard $auth
     */
    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) {
        // $signature = $request->signature;
        // $secret_key = env('API_SECRET_KEY');
        // $valid = $this->verify_signature($request);
        //
        // if ($valid['status'] == 0) {
        //     $data = array(
        //         'status'    => 0,
        //         'msg'       => 'Invalid Signature',
        //         'signature' => $valid['signature'],
        //         'signature_from_mobile' => $signature
        //     );
        //     return response($data);
        // }
        return $next($request);
    }

    public function verify_signature($request) {
        $valid = 0;
        $signature = request('signature');

        // match request based on call method
        if ($request->isMethod('get') || $request->isMethod('delete')) {
            $real_signature = md5(env('API_SECRET_KEY'));
        } else {
            // params to be excluded in md5 signature
            $exclude_params = array_merge(['q', 'signature', '_method', 'method', 'log_id', 'exclude'], explode(',', $request->exclude));
            $str = implode('', $request->except($exclude_params));
            // dd($str);
            // dd(env('API_SECRET_KEY'));
            // dd($str.env('API_SECRET_KEY'));

            // return response([$str]);
            // exit();
            $real_signature = md5($str.env('API_SECRET_KEY'));
        }

        if(@$real_signature && @$signature == @$real_signature) $valid = 1;
        return ['status' => $valid, 'signature' => $real_signature];
    }
}
