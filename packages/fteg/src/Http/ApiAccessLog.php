<?php

namespace Fteg;

use Illuminate\Database\Eloquent\Model;

class ApiAccessLog extends Model
{
    protected $table = 'ApiAccessLogMaster';
    protected $guarded = [];
}
