<?php

namespace Fteg\PublicSite\Controllers;

use Vanguard\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Fteg\Http\Requests\PublicSite\AboutUsEmailRequest;

use DB;
use View;
use DataTables;
use Carbon\Carbon;
use Mail;
// use Fteg\Announcement\Repositories\EloquentAnnouncement;
// use Fteg\News\Repositories\EloquentNews;
// use Fteg\Company\Repositories\EloquentCompany;

class PublicSiteController extends Controller
{
    protected function register_middleware() {
        $this->middleware('web');
    }

    public function __construct()
    {
        $this->register_middleware();
    }

    public function index(){
        return view('public_site::home.index');
    }

    public function sendEmail(AboutUsEmailRequest $request){

        $input = $request->all();

        Mail::send('public_site::home.about_us_email', compact('input'), function ($message){
            $message->from(env('NOREPLY_EMAIL'));
            $message->to(env('EMAIL_TO'));
            $message->subject("[Beelongz] - Inquiries");
        });
        return redirect('/#contactus')->with('success', 'Thank you for your request. We will get back to you shortly.');
    }


    // public function contact_us_email(Request $request){

    //     $name = $request->input('name');
    //     $phone = $request->input('phone');
    //     $subject = $request->input('subject');
    //     $message = $request->input('message');

    //     $content = ("

    //                     <div style='text-align:center'>
    //                      <div><h1 >Inquiries</h1></div>
    //                     </div>  


    //                     <div style='padding-left:2em;padding-top:2em;padding-right:2em'>
    //                         <p>From Mr/Ms.  : $name </p>
    //                         <p>phone number : $phone</p>
    //                         <p>subject      : $subject</p>
    //                         <br>
    //                         <br>
    //                         <p style='text-align:justify'> &emsp; $message</p><br />

    //                         </p>

    //                         <br /><br /><br />
    //                     </div>

    //         ");

    //     send_mail(env('NOREPLY_EMAIL'), 'wlxliangxian@hotmail.com', 'Inquiries', 'public_site::home.contact_us_email'
    //         , $content);

    //     // return view('public_site::home.contactUs')->with('success' , 'your email has been sent');
    //     return back()->with('success', 'Thank you for your request. We will get back to you shortly.');

    // }

}
