<?php

namespace Fteg\Http\Requests\PublicSite;

use Vanguard\Http\Requests\Request;

class AboutUsEmailRequest extends Request
{

    protected $redirect = '/#contactus';
    public function rules()
    {
        return [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|email',
            'phone' => ['required','regex:/^(00|\+)[1-9]{1}([0-9][\s]*){9,16}$/'],
            'message'=>'required'
        ];
    }

    public function messages()
    {
       return [
       ];
    }
}
