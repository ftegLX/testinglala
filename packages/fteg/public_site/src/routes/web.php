<?php

Route::group(['namespace'=>'Fteg\PublicSite\Controllers'], function(){

	//web
	if(@$_SERVER['HTTP_HOST'] == env('WEB_DOMAIN') || @$_SERVER['HTTP_HOST'] == 'www.'.env('WEB_DOMAIN') ) {

		Route::get('/', 'PublicSiteController@index');
		Route::post('/about_us_send', 'PublicSiteController@sendEmail');

	}


});
