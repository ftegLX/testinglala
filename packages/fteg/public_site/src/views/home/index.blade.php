<!DOCTYPE html>
<html lang="en">
<head>
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="csrf-token" content="{!! csrf_token() !!}">
<title>Beelongz</title>
<link href="{{ url('favicon.ico')}}" rel="icon">

<!--Plugins css-->
<link rel="stylesheet" href="assets/web_frontend/css/plugins.css">

<!--Custom Styles-->
<link rel="stylesheet" href="assets/web_frontend/css/style.css">

</head>
<style type="text/css">
  .custom-css-left{
    padding-left: 5em;
  }
  .custom-css-right{
    padding-right: 5em;
  }
  .tp-bullet.selected{
    visibility: hidden !important;
  }
  .tp-bullet {
    visibility: hidden !important;
  }

  @media only screen and (max-width: 600px) {
    .custom-css-left{
    padding-left: 0em;
  }
}
</style>
<body data-spy="scroll" data-target=".navbar" data-offset="90">

<!--PreLoader-->
<div class="loader">
   <div class="loader-inner">
      <div class="loader-blocks">
         <span class="block-1"></span>
         <span class="block-2"></span>
         <span class="block-3"></span>
         <span class="block-4"></span>
         <span class="block-5"></span>
         <span class="block-6"></span>
         <span class="block-7"></span>
         <span class="block-8"></span>
         <span class="block-9"></span>
         <span class="block-10"></span>
         <span class="block-11"></span>
         <span class="block-12"></span>
         <span class="block-13"></span>
         <span class="block-14"></span>
         <span class="block-15"></span>
         <span class="block-16"></span>
      </div>
   </div>
</div>
<!--PreLoader Ends-->

<!-- header -->
<header class="site-header">
   <nav class="navbar navbar-expand-lg center-brand static-nav">
      <div class="container">
         <a class="navbar-brand" href="/">
         <img src="assets/img/logo-transparent.png" alt="logo" class="logo-default">
         <img src="assets/img/logo-colour.png" alt="logo" class="logo-scrolled">
         </a>
         <button class="navbar-toggler navbar-toggler-right collapsed" type="button" data-toggle="collapse" data-target="#xenav">
            <span> </span>
            <span> </span>
            <span> </span>
         </button>
         <div class="collapse navbar-collapse" id="xenav">
            <ul class="navbar-nav" id="container">
               <li class="nav-item custom-css-left">
                  <a class="nav-link" href="#banner-main">Home</a>
               </li>
               <li class="nav-item custom-css-left">
                  <a class="nav-link" href="#about">About</a>
               </li>
            </ul>
            <ul class="navbar-nav ml-auto">
               <li class="nav-item custom-css-right">
                  <a class="nav-link" href="#our-App">Our App</a>
               </li>
               <!-- <li class="nav-item">
                  <a class="nav-link" href="#our-pricings">Pricing</a>
               </li> -->
               <li class="nav-item custom-css-right">
                  <a class="nav-link" href="#contactus">contact  </a>
               </li>
            </ul>
         </div>
      </div>

      <!--side menu open button-->
      <a href="javascript:void(0)" class="d-none d-lg-inline-block sidemenu_btn" id="sidemenu_toggle">
          <span></span> <span></span> <span></span>
       </a>
   </nav>

   <!-- side menu -->
   <div class="side-menu">
      <div class="inner-wrapper">
         <span class="btn-close" id="btn_sideNavClose"><i></i><i></i></span>
         <nav class="side-nav w-100">
            <ul class="navbar-nav">
               <li class="nav-item">
                  <a class="nav-link active" href="#banner-main">Home</a>
               </li>
               <li class="nav-item">
                  <a class="nav-link" href="#about">About</a>
               </li>
               <li class="nav-item">
                  <a class="nav-link" href="#our-App">Our App</a>
               </li>
               <!-- <li class="nav-item">
                  <a class="nav-link" href="#portfolio_top">Pricing</a>
               </li> -->
               <li class="nav-item">
                  <a class="nav-link" href="#contactus">Contact Us</a>
               </li>
            </ul>
         </nav>

         <div class="side-footer w-100">
            <ul class="social-icons-simple white top40">
               <li><a href="https://www.facebook.com/beelongz/" target="_blank"><i class="fa fa-facebook"></i> </a> </li>
            </ul>
            <p class="whitecolor">&copy; Copyright FTEG Technology Sdn Bhd. All Rights Reserved</p>
         </div>
      </div>
   </div>
   <a id="close_side_menu" href="javascript:void(0);"></a>
   <!-- End side menu -->
</header>
<!-- header -->



<!--Main Slider-->
<div id="revo_main_wrapper" class="rev_slider_wrapper fullwidthbanner-container">
    <div id="banner-main" class="rev_slider fullwidthabanner" style="display:none;" data-version="5.4.1">
        <ul>
            <li data-index="rs-03" data-transition="fade" data-slotamount="default" data-easein="Power3.easeInOut" data-easeout="Power3.easeInOut" data-masterspeed="2000" data-fsmasterspeed="1500">
                <!-- MAIN IMAGE -->
                <img src="assets/img/banner.jpg" alt="" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-bgparallax="10" data-no-retina>

                <div class="tp-caption tp-resizeme"
                  data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                  data-y="['middle','middle','middle','middle']" data-voffset="['-140','-140','-140','-140']"
                  data-whitespace="nowrap" data-responsive_offset="on"
                  data-width="['none','none','none','none']" data-type="text"
                  data-textalign="['center','center','center','center']"
                  data-transform_idle="o:1;"
                  data-transform_in="x:-50px;opacity:0;s:2000;e:Power3.easeOut;"
                  data-transform_out="s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;"
                  data-start="1000" data-splitin="none" data-splitout="none">
                    <h1 class="text-capitalize font-xlight whitecolor">Manage Your</h1>
                </div>
                <div class="tp-caption tp-resizeme"
                   data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                   data-y="['middle','middle','middle','middle']" data-voffset="['-70','-70','-70','-70']"
                   data-whitespace="nowrap" data-responsive_offset="on"
                   data-width="['none','none','none','none']" data-type="text"
                   data-textalign="['center','center','center','center']"
                   data-transform_idle="o:1;" data-transform_in="z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;s:1500;e:Power3.easeInOut;" data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
                   data-start="1200" data-splitin="none" data-splitout="none">
                    <h1 class="text-capitalize fontbold whitecolor">Cilent And Business</h1>
                </div>
                <div class="tp-caption tp-resizeme whitecolor"
                   data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                   data-y="['middle','middle','middle','middle']" data-voffset="['70','70','70','70']"
                   data-whitespace="nowrap" data-responsive_offset="on"
                   data-width="['none','none','none','none']" data-type="text"
                   data-textalign="['center','center','center','center']" data-fontsize="['24','24','20','20']"
                   data-transform_idle="o:1;"
                   data-transform_in="z:0;rX:0deg;rY:0;rZ:0;sX:2;sY:2;skX:0;skY:0;opacity:0;s:1000;e:Power2.easeOut;"
             data-transform_out="s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;"
             data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
                   data-start="1500" data-splitin="none" data-splitout="none">
                    <h4 class="whitecolor font-light text-center">What Can A Management System Do?</h4>
                </div>
   <!--              <div class="tp-caption tp-resizeme"
                   data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                   data-y="['middle','middle','middle','middle']" data-voffset="['160','160','160','160']"
                   data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:1500;e:Power4.easeInOut;" data-transform_out="s:900;e:Power2.easeInOut;s:900;e:Power2.easeInOut;" data-start="2000" data-splitin="none" data-splitout="none" data-responsive_offset="on">
                    <a class="button btnprimary-alt pagescroll" href="#our-process">Learn More</a>
                </div> -->

            </li>

        </ul>
    </div>
</div>
<!--Main Slider ends -->

<!--Some Feature -->
<section id="about" class="padding single-feature">
   <div class="container">
      <div class="row">
         <div class="col-md-6 col-sm-7 text-md-left text-center wow fadeInLeft" data-wow-delay="300ms">
            <div class="heading-title heading_space">
               <span style="color:#ff5f6d">Do you meet these kind of problems?</span>
               <h2 class="darkcolor bottom30">Your Problems</h2>
            </div>
            <p class="bottom35">1)Member data messy? Still record on paper?<br>
            2)Members always lost their cards? Hard to trace member packages?<br>
            3)Hard to promote to customer? Want to share latest news?</p>
         </div>
         <div class="col-md-6 col-sm-5 wow fadeInRight" data-wow-delay="350ms">
            <div class="image top50"><img alt="SEO" src="assets/img/about.png"></div>
         </div>
      </div>
   </div>
</section>
<!--Some Feature ends-->

 <!--Gallery-->
<section id="our-App" class="bglight">
   <div class="container">
      <div id="portfolio-measonry" class="cbp border-portfolio simple_overlay">
         <div class="cbp-item itemshadow">
            <img src="assets/img/car_salon.jpg" alt="">
            <!-- <div class="overlay center-block whitecolor">
               <a class="plus" data-fancybox="gallery" href="assets/template_images/portfolio-1.jpg"></a>
               <h4 class="top30">Wood Work</h4>
               <p>Small Portfolio Detail Here</p>
            </div> -->
         </div>
         <div class="cbp-item">
            <div class="text_wrap wow fadeIn" data-wow-delay="350ms">
               <div class="heading-title text-center padding_top">
                  <span  style="color:#ff5f6d">Our Service</span>
                  <h2 class="darkcolor bottom10">System Management</h2>
                  <p>Anyone who need a system to manage your works.Or a membership for your customers. </p>
               </div>
            </div>
         </div>
         <div class="cbp-item itemshadow">
            <img src="assets/img/hair.jpg" alt="">
            <!-- <div class="overlay center-block whitecolor">
               <a class="plus" data-fancybox="gallery" href="https://www.youtube.com/watch?v=_sI_Ps7JSEk&autoplay=1&rel=0&controls=0&showinfo=0"></a>
               <h4 class="top30">Wood Work</h4>
               <p>Small Portfolio Detail Here</p>
            </div> -->
         </div>
         <div class="cbp-item itemshadow">
            <img src="assets/img/pet.jpg" alt="">
            <!-- <div class="overlay center-block whitecolor">
               <a class="plus" data-fancybox="gallery" href="assets/template_images/portfolio-3.jpg"></a>
               <h4 class="top30">Wood Work</h4>
               <p>Small Portfolio Detail Here</p>
            </div> -->
         </div>
         <div class="cbp-item itemshadow">
            <img src="assets/img/nail.jpg" alt="">
            <!-- <div class="overlay center-block whitecolor">
               <a class="plus" data-fancybox="gallery" href="assets/template_images/portfolio-4.jpg"></a>
               <h4 class="top30">Wood Work</h4>
               <p>Small Portfolio Detail Here</p>
            </div> -->
         </div>
         <div class="cbp-item">
            <div class="bottom-text">
               <div class="cells  wow fadeIn" data-wow-delay="350ms">
                  <p>You can be Car Salon, Hair Salon,<br> Pet Salon, Nail Salon, Playland etc</p>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>
<!--Gallery ends -->


<!-- Mobile Apps -->
<section id="our-apps" class="padding">
   <div class="container">
      <div class="row">
         <div class="col-md-12 col-sm-12 text-center">
            <div class="heading-title wow fadeInUp" data-wow-delay="300ms">
               <span  style="color:#ff5f6d">Merchant & Member</span>
               <h2 class="darkcolor heading_space">Mobile Applications</h2>
            </div>
         </div>
      </div>
      <div class="row" id="app-feature">
         <div class="col-lg-4 col-md-4 col-sm-12">
            <div class="content-left clearfix">
               <div class="feature-item left top30 bottom30 wow fadeInUp" data-wow-delay="300ms">
                  <span class="icon"><img src="assets/img/membership.png" style="width: 40px;height: 40px" alt=""></span>
                  <div class="text">
                     <h4>Membership</h4>
                     <p>Customer data well stored and managed. Membership Card & Member Level for every member.</p>
                  </div>
               </div>
               <div class="feature-item left top30 bottom30 wow fadeInUp" data-wow-delay="350ms">
                  <span class="icon"></i><img src="assets/img/package.png" style="width: 40px;height: 40px" alt=""></span>
                  <div class="text">
                     <h4>Package</h4>
                     <p>Manage & trace the usage of member packages effortlessly</p>
                  </div>
               </div>
            </div>
         </div>
         <div class="col-lg-4 col-md-4 col-sm-12">
            <div class="image feature-item text-center  wow fadeIn" data-wow-delay="500ms">
               <img src="assets/img/mobile.png" alt="">
            </div>
         </div>
         <div class="col-lg-4 col-md-4 col-sm-12">
            <div class="content-right clearfix">
               <div class="feature-item right top30 bottom30 wow fadeInUp" data-wow-delay="300ms">
                  <span class="icon"></i><img src="assets/img/announcement.png" style="width: 40px;height: 40px" alt=""></span>
                  <div class="text">
                     <h4>Announcement</h4>
                     <p>With one-click, you can share latest news & promotions to your customers</p>
                  </div>
               </div>
               <div class="feature-item right top30 bottom30 wow fadeInUp" data-wow-delay="350ms">
                  <span class="icon"></i><img src="assets/img/notification.png" style="width: 40px;height: 40px" alt=""></span>
                  <div class="text">
                     <h4>Notification </h4>
                     <p>Notify and remind customer instantly.</p>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>
<!--Mobile Apps ends-->



<!-- Contact US -->
<section id="contactus" class="padding_top">
   <div class="container">
      <div class="row">
        <div class="col-md-12 col-sm-12">
           <div class="heading-title heading_space wow fadeIn" data-wow-delay="300ms">
               <span  style="color:#ff5f6d">Lets Get In Touch</span>
               <h2 class="darkcolor">Contact US</h2>
            </div>
        </div>
         <div class="col-md-6 col-sm-12 margin_bottom wow fadeInUp" data-wow-delay="350ms">
            <div class="row">
               <div class="col-md-6 col-sm-6 our-address top40">
                  <h5 class="bottom25">Our Address</h5>
                  <p class="bottom15">D-3-10, Seri Gembira Avenue, No.6 Jalan Senang Ria, Taman Gembira,58200 Kuala Lumpur. </p>
               </div>
               <div class="col-md-6 col-sm-6 our-address top40">
                  <h5 class="bottom25">Our Phone</h5>
                  <p class="bottom15">+ 603 7971 6365<span class="block">
                  </span> </p>
               </div>
               <div class="col-md-6 col-sm-6 our-address top40">
                  <h5 class="bottom25">Our Email</h5>
                  <p class="bottom15">info@beelongz.com</p>
               </div>
               <div class="col-md-6 col-sm-6 our-address top40">
                  <h5 class="bottom25">Our Facebook</h5>
                  <p class="bottom15">www.facebook.com/beelongz/</p>
               </div>
            </div>
         </div>
         <div class="col-md-6 col-sm-12 margin_bottom">
          @include('partials.messages')
            <form class="getin_form wow fadeInUp" data-wow-delay="400ms" action="/about_us_send"  method="post">
              @csrf
               <div class="row">

                  <div class="col-sm-12" id="result"></div>

                  <div class="col-md-6 col-sm-6">
                     <div class="form-group bottom35">
                        <input class="form-control" type="text" placeholder="First Name:" required id="first_name" name="first_name">
                     </div>
                  </div>
                  <div class="col-md-6 col-sm-6">
                     <div class="form-group bottom35">
                        <input class="form-control" type="text" placeholder="Last Name:" required id="last_name" name="last_name">
                     </div>
                  </div>
                  <div class="col-md-6 col-sm-6">
                     <div class="form-group bottom35">
                        <input class="form-control" type="email" placeholder="Email:" required id="email" name="email">
                     </div>
                  </div>
                  <div class="col-md-6 col-sm-6">
                     <div class="form-group bottom35">
                        <input class="form-control" type="tel" placeholder="Phone:" id="phone" name="phone">
                     </div>
                  </div>
                  <div class="col-md-12 col-sm-12">
                     <div class="form-group bottom35">
                        <textarea class="form-control" placeholder="Message" id="message" name="message"></textarea>
                     </div>
                  </div>
                  <div class="col-sm-12">
                     <button type="submit" class="button btnprimary" id="submit_btn">submit request</button>
                  </div>
               </div>
            </form>
         </div>
      </div>
   </div>

   <!--Location Map here-->
   <span class="metadata-marker" style="display: none;" data-region_tag="html-body"></span>
</section>
<div id="map" style="height: 450px;width: 100%"></div>
<!--Contact US Ends-->

<!--Site Footer Here-->
<footer id="site-footer" class="padding_half">
   <div class="container">
      <div class="row">
         <div class="col-md-12 col-sm-12 text-center">
            <ul class="social-icons bottom25 wow fadeInUp" data-wow-delay="300ms">
               <li><a href="https://www.facebook.com/beelongz/" target="_blank"><i class="fa fa-facebook"></i> </a> </li>
            </ul>
            <p class="copyrights wow fadeInUp" data-wow-delay="350ms"> &copy; Copyright FTEG Technology Sdn Bhd. All Rights Reserved </p>
         </div>
      </div>
   </div>
</footer>
<!--Footer ends-->


<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="assets/web_frontend/js/jquery-3.1.1.min.js"></script>

<!--Bootstrap Core-->
<script src="assets/web_frontend/js/popper.min.js"></script>
<script src="assets/web_frontend/js/bootstrap.min.js"></script>

<!--to view items on reach-->
<script src="assets/web_frontend/js/jquery.appear.js"></script>

<!--Equal-Heights-->
<script src="assets/web_frontend/js/jquery.matchHeight-min.js"></script>

<!--Owl Slider-->
<script src="assets/web_frontend/js/owl.carousel.min.js"></script>

<!--number counters-->
<script src="assets/web_frontend/js/jquery-countTo.js"></script>

<!--Parallax Background-->
<script src="assets/web_frontend/js/parallaxie.js"></script>

<!--Cubefolio Gallery-->
<script src="assets/web_frontend/js/jquery.cubeportfolio.min.js"></script>

<!--FancyBox popup-->
<script src="assets/web_frontend/js/jquery.fancybox.min.js"></script>

<!-- Video Background-->
<script src="assets/web_frontend/js/jquery.background-video.js"></script>

<!--TypeWriter-->
<script src="assets/web_frontend/js/typewriter.js"></script>

<!--Particles-->
<script src="assets/web_frontend/js/particles.min.js"></script>

<!--WOw animations-->
<script src="assets/web_frontend/js/wow.min.js"></script>

<!--Revolution SLider-->
<script src="assets/web_frontend/js/revolution/jquery.themepunch.tools.min.js"></script>
<script src="assets/web_frontend/js/revolution/jquery.themepunch.revolution.min.js"></script>
<!-- SLIDER REVOLUTION 5.0 EXTENSIONS  (Load Extensions only on Local File Systems !  The following part can be removed on Server for On Demand Loading) -->
<script src="assets/web_frontend/js/revolution/extensions/revolution.extension.actions.min.js"></script>
<script src="assets/web_frontend/js/revolution/extensions/revolution.extension.carousel.min.js"></script>
<script src="assets/web_frontend/js/revolution/extensions/revolution.extension.kenburn.min.js"></script>
<script src="assets/web_frontend/js/revolution/extensions/revolution.extension.layeranimation.min.js"></script>
<script src="assets/web_frontend/js/revolution/extensions/revolution.extension.migration.min.js"></script>
<script src="assets/web_frontend/js/revolution/extensions/revolution.extension.navigation.min.js"></script>
<script src="assets/web_frontend/js/revolution/extensions/revolution.extension.parallax.min.js"></script>
<script src="assets/web_frontend/js/revolution/extensions/revolution.extension.slideanims.min.js"></script>
<script src="assets/web_frontend/js/revolution/extensions/revolution.extension.video.min.js"></script>

<!--Google Map API-->
<!-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA_egfOLjahHB0IWpykRZrVFD8fN4JMgmw"></script> -->

<script>

var map;
function initMap() {
  map = new google.maps.Map(document.getElementById('map'), {
    center: {lat: 3.079209, lng: 101.686994},
    zoom: 18
  });
  var marker = new google.maps.Marker({
    position: {lat: 3.079209, lng: 101.686994},
    map: map,
  });
}

</script>

<script async defer src="https://maps.googleapis.com/maps/api/js?key={{ getenv('MAP_API') }}&callback=initMap" ></script>

<script src="assets/web_frontend/js/functions.js"></script>
</body>
</html>
