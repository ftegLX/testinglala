<html>
    <body>
        <div style='background-color:#f6f6f6;padding-bottom:10px;'>
            <div style='text-align:center;padding-top:2em;'>
                <img style='width:100px;height:auto' src="{{ 'http://'.env('ADMIN_DOMAIN') }}/assets/img/logo.png">
            </div>

            <div style='background-color:white;text-align:left;
            width:500px;margin-left:auto;margin-right:auto;
            border:1px solid lightgrey;margin-top:2em'>
                <div style='text-align:center'>
                        <div><h1 >Inquiries</h1></div>
                </div>
                <div style='padding-left:1em'>
                    <p>From Mr/Ms.  : {{$input['last_name'].' '.$input['first_name']}} </p>
                    <p>Phone number : {{$input['phone']}}</p>
                    <br>
                    <br>
                    <p style='text-align:justify'> &emsp; {{$input['message']}}</p><br />

                    </p>

                    <br /><br /><br />
                </div>
            </div>

            <br /><br />

            <div style='margin-left:auto;margin-right:auto;width:500px;'>
                <br />
                <p style='font-style: italic;color:grey'>
                 Warmest regards,</p>
                <p style='font-style: italic;color:grey'>
                Team</p>
            </div>

            <br /><br /><br />
            <br /><br /><br />
        </div>
    </body>
</html>
