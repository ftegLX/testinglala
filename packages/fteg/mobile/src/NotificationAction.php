<?php

namespace Fteg;

class NotificationAction {
    // member app
    const MEMBERSHIP_INVITATION = 'membership-invitation';
    const MERCHANT_WELCOME = 'merchant-welcome';
    const ANNOUNCEMENT_DETAILS = 'announcement-details';
    const MERCHANT_LANDING = 'merchant-landing';
    const PACKAGE_LISTING = 'package-listing';
    const PACKAGE_HISTORY = 'package-history';
    const BOOKING_LISTING = 'booking-listing';
    const BOOKING_DETAILS = 'booking-details';

    // merchant app
    const MEMBER_PROFILE = 'member-profile';
}
