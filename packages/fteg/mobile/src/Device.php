<?php

namespace Fteg;

use Illuminate\Database\Eloquent\Model;

class Device extends Model
{
    protected $table = 'DeviceMaster';
    protected $fillable = ['user_id', 'uuid', 'brand', 'modal', 'os' ,'os_version', 'device_token',
                            'app_version', 'in_use', 'token', 'merchant_id'];
    // protected $guarded = [];

    CONST APP_TYPE_MEMBER = 'member';
    CONST APP_TYPE_MERCHANT = 'merchant';
}
