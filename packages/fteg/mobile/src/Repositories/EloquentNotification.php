<?php

namespace Fteg\Mobile\Repositories;

use Schema;
use Fteg\Merchant;
use Vanguard\User;
use Fteg\NotificationAction;
use Fteg\NotificationHistory;

use Illuminate\Http\Request;

class EloquentNotification
{
    public function setUp() {
        $this->create_notification_history_table();
    }

    public function list_user(Request $request, User $user, $paginate = false) {
        $query = NotificationHistory::where('user_id', $user->id)
                    ->where('status', NotificationHistory::STATUS_ACTIVE)
                    ->where('target', NotificationHistory::TARGET_MEMBER)
                    ->orderBy('created_at', 'desc');

        if ($request->has('merchant_id')) {
            $query->where('type', NotificationHistory::TYPE_MERCHANT)
                ->where('merchant_id', $request->merchant_id);
        } else {
            // get default notifications
            $query->where('type', NotificationHistory::TYPE_GENERAL);
        }

        $query->orWhere('user_id', 0);

        if ($paginate)
            return $query->paginate(10);

        return $query->get();
    }

    public function list_user_unread_count(User $user) {
        return NotificationHistory::where('user_id', $user->id)
            ->where('status', NotificationHistory::STATUS_ACTIVE)
            ->where('target', NotificationHistory::TARGET_MEMBER)
            ->where('type', NotificationHistory::TYPE_GENERAL)
            ->where('read', false)
            ->count();
    }

    public function list_member_unread_count(User $user, Merchant $merchant) {
        return NotificationHistory::where('user_id', $user->id)
            ->where('status', NotificationHistory::STATUS_ACTIVE)
            ->where('target', NotificationHistory::TARGET_MEMBER)
            ->where('type', NotificationHistory::TYPE_MERCHANT)
            ->where('merchant_id', $merchant->id)
            ->where('read', false)
            ->count();
    }

    public function list_merchant_unread_count(User $user) {
        return NotificationHistory::where('user_id', $user->id)
            ->where('status', NotificationHistory::STATUS_ACTIVE)
            ->where('target', NotificationHistory::TARGET_MERCHANT)
            ->where('read', false)
            ->count();
    }

    public function list_merchant(Request $request, User $user, $paginate = false) {
        $query = NotificationHistory::where('user_id', $user->id)
                    ->where('status', NotificationHistory::STATUS_ACTIVE)
                    ->where('target', NotificationHistory::TARGET_MERCHANT)
                    ->orderBy('created_at', 'desc');

        if ($paginate)
            return $query->paginate(10);

        return $query->get();
    }

    public function hide_invitation_notifications(User $user) {
         NotificationHistory::where('target', NotificationHistory::TARGET_MEMBER)
            ->where('action', NotificationAction::MEMBERSHIP_INVITATION)
            ->where('user_id', $user->id)
            ->update(['status' => NotificationHistory::STATUS_REMOVED]);
    }
}
