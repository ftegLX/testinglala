<?php
namespace Fteg\Mobile\Controllers;

use Vanguard\Http\Controllers\Controller;
use Illuminate\Http\Request;


use Fteg\AppNotification;
use Fteg\NotificationHistory;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;
use Vanguard\Serializers\CustomSerializer;

use Fteg\Mobile\Repositories\EloquentNotification;
use Fteg\Mobile\Transformers\NotificationHistoryTransformer;

class ApiNotificationController extends Controller
{
    protected $notificationRepo;
    protected $manager;

    protected function register_middleware() {
        $this->middleware(['api', 'maintenance']);
        $this->middleware(['receive', 'response']);
        $this->middleware('signature');
        $this->middleware('jwt');
    }

    public function __construct(Manager $manager, EloquentNotification $notificationRepo) {
        $this->register_middleware();
        $this->manager = $manager;
        $this->notificationRepo = $notificationRepo;

        $this->manager->setSerializer(new CustomSerializer());
    }

    public function create_table() {
        $this->notificationRepo->setUp();
    }

    public function user_list(Request $request) {
        $user = auth()->user();
        $col = $this->notificationRepo->list_user($request, $user, true);
        $resource = new Collection($col->all(), new NotificationHistoryTransformer());
        $list = $this->manager->createData($resource)->toArray();
        $total_page = $col->lastPage();
        return ['status' => 1, 'msg' => 'Valid', 'list' => $list, 'total_page' => $total_page];
    }

    public function merchant_list(Request $request) {
        $user = auth()->user();
        $col = $this->notificationRepo->list_merchant($request, $user, true);
        $resource = new Collection($col->all(), new NotificationHistoryTransformer());
        $list = $this->manager->createData($resource)->toArray();
        $total_page = $col->lastPage();
        return ['status' => 1, 'msg' => 'Valid', 'list' => $list, 'total_page' => $total_page];
    }

    public function read(NotificationHistory $notification) {
        $notification->hasBeenRead();
        return ['status' => 1, 'msg' => 'Valid'];
    }

}
