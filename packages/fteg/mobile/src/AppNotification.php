<?php
namespace Fteg;

use Fteg\Device;
use Fteg\Merchant;
use Vanguard\User;
use Fteg\NotificationHistory;

use Carbon\Carbon;

class AppNotification
{
    public static function push_to_users(array $user_ids, Merchant $merchant = null, $type, array $data) {
        $app_id = self::get_member_app_id($merchant);
        $rest_api_key = self::get_member_api_key($merchant);

        $query = Device::whereIn('user_id', $user_ids)->where('in_use', 1);
        if ($merchant->level == Merchant::LEVEL_WHITE_LABEL)
            $query->where('merchant_id', $merchant->id);
        else
            $query->where('merchant_id', null);

        $devices = $query->get();
        $fields = [
            'app_id' => $app_id,
            'data' => $data,
            'headings' => ["en"=>$data['title']],
            'contents' => ["en"=>$data['message']],
            'include_player_ids' => $devices->pluck('device_token')->toArray()
        ];

        $target = NotificationHistory::TARGET_MEMBER;
        $response = self::send($fields, $rest_api_key);
        self::store_history($target, $type, $devices, $merchant, $response, $data);
    }

    public static function push_to_merchants(array $user_ids, Merchant $merchant = null, $type, array $data) {
        $app_id = self::get_merchant_app_id($merchant);
        $rest_api_key = self::get_merchant_api_key($merchant);
        $devices = Device::whereIn('user_id', $user_ids)->where('in_use', 1)->get();

        $fields = [
            'app_id' => $app_id,
            'data' => $data,
            'headings' => ["en"=>$data['title']],
            'contents' => ["en"=>$data['message']],
            'include_player_ids' => $devices->pluck('device_token')->toArray()
        ];

        $target = NotificationHistory::TARGET_MERCHANT;
        $response = self::send($fields, $rest_api_key);
        self::store_history($target, $type, $devices, $merchant, $response, $data);
    }

    protected static function get_member_app_id(Merchant $merchant = null) {
        // get white label notification app id
        if ($merchant && $merchant->level == Merchant::LEVEL_WHITE_LABEL)
            return $merchant->notification_member_app_id;

        // get notification app id from env
        return env('MEMBER_APP_ID');
    }

    protected static function get_merchant_app_id(Merchant $merchant = null) {
        // get white label notification app id
        if ($merchant && $merchant->level == Merchant::LEVEL_WHITE_LABEL)
            return $merchant->notification_merchant_app_id;

        // get notification app id from env
        return env('MERCHANT_APP_ID');
    }

    protected static function get_member_api_key(Merchant $merchant = null) {
        // get white label notification app id
        if ($merchant && $merchant->level == Merchant::LEVEL_WHITE_LABEL)
            return $merchant->notification_member_api_key;

        // get notification app id from env
        return env('MEMBER_REST_API_KEY');
    }

    protected static function get_merchant_api_key(Merchant $merchant = null) {
        // get white label notification app id
        if ($merchant && $merchant->level == Merchant::LEVEL_WHITE_LABEL)
            return $merchant->notification_merchant_api_key;

        // get notification app id from env
        return env('MERCHANT_REST_API_KEY');
    }

    protected static function send(array $fields, $rest_api_key) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
        curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json; charset=utf-8',
                                                   'Authorization: Basic '.$rest_api_key]);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

        $response = curl_exec($ch);
        curl_close($ch);
        return $response;
    }

    protected static function store_history($target, $type, $devices, Merchant $merchant = null, $response, $data) {
        $module_id = null;
        if (in_array($data['module'], ['announcement']))
            $module_id = $data['id'];

        $notifications = [];
        foreach ($devices as $device) {
            $notifications[] = [
                'user_id'       => $device->user_id,
                'merchant_id'   => @$merchant->id ?: null,
                'type'          => $type,
                'module'        => $data['module'],
                'module_id'     => $module_id,
                'action'        => $data['action'],
                'title'         => $data['title'],
                'message'       => $data['message'],
                'data'          => json_encode($data),
                'target'        => $target,
                'multicast_id'  => json_encode($device->device_token),
                'status'        => !isset(json_decode($response)->errors),
                'result'        => json_encode($response),
                'read'          => false,
                'created_at'    => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at'    => Carbon::now()->format('Y-m-d H:i:s')
            ];
        }
        NotificationHistory::insert($notifications);
    }

    public static function welcome_notification($target, $type, $user_id, $merchant_id = null, array $data) {
        NotificationHistory::create([
            'user_id'       => $user_id,
            'merchant_id'   => $merchant_id,
            'type'          => $type,
            'module'        => $data['module'],
            'action'        => $data['action'],
            'title'         => $data['title'],
            'message'       => $data['message'],
            'data'          => json_encode($data),
            'target'        => $target,
            'multicast_id'  => '',
            'status'        => NotificationHistory::STATUS_ACTIVE,
            'result'        => '',
            'read'          => false,
        ]);
    }
}
