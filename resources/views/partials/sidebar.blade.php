<nav class="col-md-2 sidebar">
    <div class="sidebar-sticky">
        <ul class="nav flex-column">
            <li class="nav-item">
                <a class="nav-link {{ Request::is('/') ? 'active' : ''  }}" href="{{ route('dashboard') }}">
                    <i class="fas fa-home"></i>
                    <span>@lang('app.dashboard')</span>
                </a>
            </li>

            {{-- <li class="nav-item">
                <a class="nav-link {{ Request::is('members*') ? 'active' : ''  }}" href="/members">
                    <i class="fas fa-user"></i>
                    <span>Members</span>
                </a>
            </li>

            <li class="nav-item">
                <a class="nav-link {{ Request::is('staffs*') ? 'active' : ''  }}" href="/staffs">
                    <i class="fas fa-user-circle"></i>
                    <span>Staffs</span>
                </a>
            </li>

            @if($modules->contains('name', 'bookings'))
                @permission('view.bookings')
                    <li class="nav-item">
                        <a class="nav-link {{ Request::is('bookings*') ? 'active' : ''  }}" href="/bookings">
                            <i class="fas fa-calendar-check"></i>
                            <span>Bookings</span>
                        </a>
                    </li>
                @endpermission
            @endif

            @if($modules->contains('name', 'announcements'))
                @permission('view.announcements')
                    <li class="nav-item">
                        <a class="nav-link {{ Request::is('announcements*') ? 'active' : ''  }}" href="/announcements">
                            <i class="fas fa-bullhorn"></i>
                            <span>Announcements</span>
                        </a>
                    </li>
                @endpermission
            @endif

            @if($modules->contains('name', 'orders'))
                @permission('view.orders')
                    <li class="nav-item">
                        <a class="nav-link {{ Request::is('orders*') ? 'active' : ''  }}" href="/orders">
                            <i class="fas fa-shopping-cart"></i>
                            <span>Orders</span>
                        </a>
                    </li>
                @endpermission
            @endif

            @if($modules->contains('name', 'packages'))
                <li class="nav-item">
                    <a href="#packages-and-services-dropdown"
                       class="nav-link"
                       data-toggle="collapse"
                       aria-expanded="{{ Request::is('packages*', 'services*') ? 'true' : 'false' }}">
                        <i class="fas fa-boxes"></i>
                        <span>Packages / Services</span>
                    </a>
                    <ul class="{{ Request::is('packages*', 'services*') ? '' : 'collapse' }} list-unstyled sub-menu" id="packages-and-services-dropdown">
                        @permission('view.packages')
                            <li class="nav-item">
                                <a class="nav-link {{ Request::is('packages*') && !Request::is('packages/sign') ? 'active' : '' }}"
                                   href="/packages">Packages</a>
                            </li>
                        @endpermission
                        @permission('view.services')
                            <li class="nav-item">
                                <a class="nav-link {{ Request::is('services*') ? 'active' : '' }}"
                                   href="/services">Services</a>
                            </li>
                        @endpermission
                    </ul>
                </li>
            @endif

            @permission('edit.merchant.information')
                <li class="nav-item">
                    <a class="nav-link {{ Request::is('branches*') ? 'active' : ''  }}" href="/branches">
                        <i class="fas fa-store-alt"></i>
                        <span>Branches</span>
                    </a>
                </li>
            @endpermission

            @permission('edit.merchant.information')
            <li class="nav-item">
                <a href="#settings-dropdown"
                   class="nav-link"
                   data-toggle="collapse"
                   aria-expanded="{{ Request::is('tiers*') ? 'true' : 'false' }}">
                    <i class="fas fa-cog"></i>
                    <span>Settings</span>
                </a>
                <ul class="{{ Request::is('tiers*', 'about-us') ? '' : 'collapse' }} list-unstyled sub-menu" id="settings-dropdown">
                    <li class="nav-item">
                        <a class="nav-link {{ Request::is('tiers*') ? 'active' : '' }}"
                           href="/tiers">Membership Tiers</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link {{ Request::is('about-us') ? 'active' : '' }}"
                           href="/about-us">Merchant Details</a>
                    </li>
                </ul>
            </li>
            @endpermission --}}
        </ul>
    </div>
</nav>
